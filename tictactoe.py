b=[]
score=0
for e in range(3):
    r=[]
    for e5 in range (3):
        r.append('[  ]')
    b.append(r)
def printboard(b):
    for r in range (3):
        for c in range (3):
            print (b[r][c], end="")
        print()
    return
def turn(player):
    while True:
        machikoro=input("Where do want to place your marker? Give your answers in row letter, column number form.")
        r,c=machikoro.split(",")
        print(c)
        print(r)
        c=int(c)-1
        if r=="a":
            r=0
        elif r=="b":
            r=1
        else:
            r=2
        if b[r][c]!='[  ]':
            print("You can't put your marker there.")
            continue
        b[r][c]=player
        
        printboard(b)
        return
def checkwin(player):
    if b[0][0]==player and b[1][1]==player and b[2][2]==player:
        print("Player " + str(player) + "has won!")
        exit()
    elif b[0][2]==player and b[1][1]==player and b[2][0]==player:
        print("Player " + str(player) + "has won!")        
        exit()
    elif b[0][2]==player and b[1][2]==player and b[2][2]==player:
        print("Player " + str(player) + "has won!")        
        exit()
    elif b[0][1]==player and b[1][1]==player and b[2][1]==player:
        print("Player " + str(player) + "has won!")        
        exit()
    elif b[0][0]==player and b[0][1]==player and b[0][2]==player:
        print("Player " + str(player) + "has won!")        
        exit()
    elif b[1][2]==player and b[1][1]==player and b[1][0]==player:
        print("Player " + str(player) + "has won!")        
        exit()
    elif b[2][2]==player and b[2][1]==player and b[2][0]==player:
        print("Player " + str(player) + "has won!")        
        exit()
    elif b[0][0]==player and b[1][0]==player and b[2][0]==player:
        print("Player " + str(player) + "has won!")        
        exit()

score=0
while True:
    turn("[ X ]")
    checkwin("[ X ]")
    score=score+1
    turn("[ O ]")
    checkwin("[ O ]")
    score=score+1    