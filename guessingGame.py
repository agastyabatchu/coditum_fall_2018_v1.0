'''
Created on Sep 13, 2018

@author: coditum
'''
print("You are a Portugese spy who has special information about Spain in an execution chamber in 1522 Madrid, trying to get to Córdoba.")
print("You see a chance to escape as the guards are drunk.")
a1=input("Do you 1: Attempt to escape, 2: Hide or 3: Stay to be killed?")
if a1=="3":
    print("Charlie Brown the Executioner and Lindsay the Executioner run a sword through your corpse. YOU DIE.")
    exit()
if a1=="2":
    print("You are now hiding in your execution chamber. Charlie Brown the Executioner comes and doesn't see you.")
    print("He says that you have escaped, and he runs out with his drunk guards. Lindsay the Executioner, however, is hooked in a conversation with a Knight just outside of the room.")
    b1=input("Do you 1: Sneak past, 2: Keep hiding or 3: Run away!?")
    if b1=="3":
        print("You get noticed as you are running. One soldier that is not drunk comes at you and stabs you in the pelvis with his spear. YOU DIE.")
        exit()
    if b1=="2":
        print("You keep hiding in the execution chamber. Lindsay the executioner and the Knight come into the chamber and search the place. They find you first thing and you are killed. YOU DIE.")
        exit()
    else:
        b2=input("You sneak past Lindsay while she enters the room with the knight. Do you 1: start running or 2: keep sneaking? ")
        if b2=="2":
            print("As you are sneaking, a Spanish official passing by in the hall shouts, and Lindsay comes. She takes a 2m sword and stabs you in the bottom. Blood splatters all over. YOU DIE.")
            exit()
        else: 
            print("You are running for your life from the prison. You are in the outskirts of Madrid, and you know that the safest way to get to Córdoba is by getting a horse from Madrid and riding away on it.")
    a2=input("Do you 1: take Route 5, a shortcut that is likely to have Spaniards, or 2: Route 6, a roadthat will take longer but has less soldiers?")
else:
    print("You are running for your life from the prison. You are in the outskirts of Madrid, and you know that the safest way to get to Córdoba is by getting a horse from Madrid and riding away on it.")
    a2=input("Do you 1: take Route 5, a shortcut that is likely to have Spaniards, or 2: Route 6, a road that will take longer but has less soldiers?")
if a2=="2":
    print("You are riding down Route 6. But a platoon of troops is behind you. They shoot your horse and your horse dies.")
    ab36c=input("You fall down but can still run. Do you 1: Stop or 2: Run?")
    if ab36c=="1":
        print("You are off the horse, on the ground. The Spanish troops are right next to you. They take out their swords, strip you, and cut you open. You are drawn, then quartered. YOU DIE.")
        exit()
    else:
        print("You are running on Route 6 to Madrid. The Spanish troops shoot you from behind. Your head and sacrum area are gashing out blood. YOU DIE")
        exit()
else:
    print("Lucky you! There are no Spanish soldiers on Route 5 duty today. You keep on riding on the horse into Madrid. You arrive at a junction. There are multiple roads.")
    a3=input("Do you take 1: Route 5, 2: Route 6, 3: Route 860 or 4: Route 66?")
    if a3=="1":
        print("You notice that there is a dead soldier with a gun in his hands. You pick up the gun and take it with you. Soon, there is a squad of Spanish soldiers on the road.")
        a4=input("Do you 1: Shoot the troops or 2: Try to run over the troops?")
        if a4=="2":
            print("They shoot you first. YOU DIE.")
            exit()
        else:
            a5=input("You are on the road to Córdoba but on the exit, you notice that there are several Spanish troops. Do you 1: Shoot them and go through or 2: Keep going on the road?")
            if a5=="1":
                print("BIFF! BAFF! You kill all of the troops and stomp over the rest. You reach Córdoba and you head to your transport to Portugal. You are so happy that you have completed your mission! THE END, YOU WIN.")
                exit()
            else:
                a6=input("You keep going on the road heading southwest. You see another exit that says: Route 450-Córdoba, Granada. You can take that exit north to Córdoba. However, there are even more Spaniards on that exit. Do you 1: take the exit and stomp over the troops or 2: Stay on the road?")
                if a6=="1":
                    print("BIFF! BAFF! You kill all of the troops and stomp over the rest. You ride through Route 450 and you reach Córdoba and you head to your transport to Portugal. You are so happy that you have completed your mission! THE END, YOU WIN.")
                    exit()
                else:
                    a7=input("You are still on the road. You see an exit that says: Route 90-Málaga. Do you 1: Take the exit or 2: Continue?")
                    if a7=="1":
                        print("You take the exit. But a batallion comes after a long travel. Tired, you are shot to death. YOU DIE.")
                        exit()
                    else:
                        print("You reach Sevilla after a long travel. The police check you and realise who you are. You are shot to death. YOU DIE.")
                        exit()
    if a3=="2":
        b3=input("You are on Route 6. You see that there is a toll booth where you can get caught. Do you 1: speed through the toll or 2: go slow?")
        if b3=="2":
            print("The person at duty catches you. You are ushered out and killed on the roadside by an officer. YOU DIE.")
            exit()
        else:
            b4=input("You have gotten out of the toll booth! Now there are exits on the road. Do you 1: take the exit to Route 5, 2: Route 450 or 3: stay on the road?")
            if b4=="3":
                print("There is a roadblock that has armed Spanish troops. You get shot. YOU DIE.")
            if b4=="1":
                a5=input("You are on the road to Córdoba but on the exit, you notice that there are several Spanish troops. Do you 1: Shoot them and go through or 2: Keep going on the road?")
                if a5=="1":
                    print("BIFF! BAFF! You kill all of the troops and stomp over the rest. You reach Córdoba and you head to your transport to Portugal. You are so happy that you have completed your mission! THE END, YOU WIN.")
                    exit()
                else:
                    a6=input("You keep going on the road heading southwest. You see another exit that says: Route 450-Córdoba, Granada. You can take that exit north to Córdoba. However, there are even more Spaniards on that exit. Do you 1: take the exit and stomp over the troops or 2: Stay on the road?")
                    if a6=="1":
                        print("BIFF! BAFF! You kill all of the troops and stomp over the rest. You ride through Route 450 and you reach Córdoba and you head to your transport to Portugal. You are so happy that you have completed your mission! THE END, YOU WIN.")
                        exit()
                    else:
                        a7=input("You are still on the road. You see an exit that says: Route 90-Málaga. Do you 1: Take the exit or 2: Continue?")
                        if a7=="1":
                            print("You take the exit. But a batallion comes after a long travel. Tired, you are shot to death. YOU DIE.")
                            exit()
                        else:
                            print("You reach Sevilla after a long travel. The police check you and realise who you are. You are shot to death. YOU DIE.")
                            exit()
            else:
                a6=input("You keep going on the road heading southwest. You see another exit that says: Route 450-Córdoba, Granada. You can take that exit north to Córdoba. However, there are even more Spaniards on that exit. Do you 1: take the exit and stomp over the troops or 2: Stay on the road?")
                if a6=="1":
                    print("BIFF! BAFF! You kill all of the troops and stomp over the rest. You ride through Route 450 and you reach Córdoba and you head to your transport to Portugal. You are so happy that you have completed your mission! THE END, YOU WIN.")
                    exit()
                else:
                    a7=input("You are still on the road. You see an exit that says: Route 90-Málaga. Do you 1: Take the exit or 2: Continue?")
                    if a7=="1":
                        print("You take the exit. But a batallion comes after a long travel. Tired, you are shot to death. YOU DIE.")
                        exit()
                    else:
                        print("You reach Sevilla after a long travel. The police check you and realise who you are. You are shot to death. YOU DIE.")
                        exit()
    if a3=="3":  
        c1=input("You are on the road when you see a kitten on the road. Do you 1: pick her up or 2: leave her?")
        if c1=="1":
            print("The kitten bites you. You get stunned with slit wrists and you die on the road.")
        else:
            c2=input("It would have been a bad idea to pick up the cat. You are continuing on the road when you reach a junction with route 66. Do you 1: Transfer or 2: Stay on the road?")
            if c2=="1":
                print("The traffic police catches you and YOU DIE.")
                exit()
            else:
                c3=input("You have reached an exit to Córdoba. Do you 1: take the exit or 2: not?")
                if c3=="2":
                    print("You reached a dead end. People crash behind you. YOU DIE.")
                    exit()
                else:
                    print("You reached! YOU WIN.")
                    exit()
    if a3=="4":
        abc1=input("You get stuck in traffic. You see a sign that says Route 78 spur road exit. Do you 1:take the exit or 2: not?")
        if abc1=="2":
            print("A military trooper reaches you and you are dead meat on the street with blood all around. YOU DIE.")
            exit()
        else:
            abc2=input("You are on Route 78 now, you see the exit back to route 66. Do you 1: take the exit or 2: not?")
            if abc2=="1":
                print("The exit to route 860 doesn't come back. You pass by a military unit that begins chasing you. An arrow shoots you from behind and blood splatters all over. YOU DIE.")
                exit()
            else:
                c2=input("You are continuing on the road when you reach a junction with route 860. Do you 1: Transfer or 2: Stay on the road?")
                if c2=="2":
                    print("The traffic police catches you and YOU DIE.")
                    exit()
                else:
                    abc3==input("You have reached an exit to Córdoba. Do you 1: take the exit or 2: not?")
                    if abc3=="2":
                        print("You reached a dead end. People crash behind you. YOU DIE.")
                        exit()
                    else:
                        print("You reached! YOU WIN.")
                        exit()
