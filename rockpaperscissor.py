'''
Created on Sep 27, 2018

@author: coditum
'''
from random import randint
score=0
print("Welcome to rock paper scissors!")
while score<3:
    r=input("Pick a rock, paper or scissors.")
    if r=="gun":
        print("I don't like you but you win anyway!")
        score=score+1
    else:
        n=randint(0,2)
        if n==0:
            if r=="rock":
                print("You tied with the computer.")
            elif r=="scissors":
                print("You lost to the computer. Too bad for you!")
                score=score-1
            elif r=="paper":
                print("You won against the computer! Good.")
                score=score+1
        elif n==1:
            if r=="paper":
                print("You tied with the computer.")
            elif r=="rock":
                print("You lost to the computer. Too bad for you!")
                score=score-1
            elif r=="scissors":
                print("You won against the computer! Good.")
                score=score+1
        elif n==2:
            if r=="scissors":
                print("You tied with the computer.")
            elif r=="paper":
                print("You lost to the computer. Too bad for you!")
                score=score-1
            elif r=="rock":
                print("You won against the computer! Good.")
                score=score+1
    print("Your score is " + str(score))
    if score==3:
        print("You won!!")
    